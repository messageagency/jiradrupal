# Drupal Atlassian Integration Project #

## Project Description ##
The goal of this project is to integrate the Atlassian suite of products with Drupal, the open source CMS to help make 
this suite of products easier for clients to use both during and after development.  The main objective is to create a 
“Dashboard-like” integration with Atlassian products in Drupal, which would give a Drupal administrator a complete 
“snapshot” of their development project.  This would include the ability to create, view, and update issues that would 
normally be entered into the service desk; to view confluence spaces and pages to which the user has access, and 
rendering charts using jQuery libraries to provide an appealing visual representation of their projects’ progress (for 
example, issues resolved versus total issues, logged hours, build status and deployment updates, among others).  We 
want to design this dashboard so that a client can log in to a Drupal site and get an up-to-date synopsis of their 
project's health.  The synchronization that occurs would allow for Drupal to communicate with the Atlasssian suite of 
products to allow for updating/creating issues in Drupal that would then become Service Desk requests. Another example 
would be updating/commenting on Confluence Spaces that they have the correct permissions for. We also want to pull in 
information from Bamboo on recent deployments and build status.

## Contributors/Authors ##
https://www.drupal.org/node/2465341

## TODO: ##
Installation/Usage Instructions
Roadmap of Development Phases

## Licensing ##
GNU General Public License